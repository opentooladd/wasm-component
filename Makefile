build:
	cargo build

test-web:
	wasm-pack test --firefox --headless

coverage: install-tarpaulin
	cargo tarpaulin -v --out Html Lcov --output-dir coverage/

install-tarpaulin: ~/.cargo/bin/cargo-tarpaulin

# see deps here: https://github.com/xd009642/tarpaulin
~/.cargo/bin/cargo-tarpaulin:
	cargo install cargo-tarpaulin
