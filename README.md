# Wasm-Component

A Rust crate to create Web-Assembly component for Rust structure with a logic of Finite [State Machine](https://en.wikipedia.org/wiki/Finite-state_machine).

## How to use

To use the crate you have to implement the `DOMComponent` trait and the `Machine` trait.
The `Machine` will handle all the transition between states and handle the data manipulation, while the `DOMComponent` represents the interface for a `Machine` to interact with a `Driver`.

For the moment there is only 1 `Driver` type and 1 `App` type.
But the logic is to be able to expand this logic to other graphical environment than the DOM (ex: Piston, App, Desktop...)
