use wasm_bindgen::JsCast;
use crate::v2::driver::dom::AppEvent;
use crate::v2::driver::dom::Driver;
use crate::v2::driver::dom::component::DOMComponent;
use crate::v2::driver::dom::{ log, error, document };

pub struct App<M: DOMComponent> {
   driver: Driver<M>,
}

impl<C: DOMComponent> App<C> {
   pub fn new(shadow: bool) -> Self {
      App {
         driver: Driver::new(shadow),
      }
   }
    pub fn mount(
       mut self, 
       element: web_sys::Element,
     ) -> gloo::events::EventListener {
        let window = web_sys::window().unwrap();
        clear_element(&element);
        self.driver.mount(None, self.driver.shadow);

        // log(format!("from app init: {}_app_message", app.id.clone()).as_str());
        let listener = gloo::events::EventListener::new(&window, format!("{}_app_message", "App"), move |arg| {
            let e = arg.clone().dyn_into::<web_sys::CustomEvent>().ok().unwrap();
            let mess: Result<AppEvent, serde_json::error::Error> = e.detail().into_serde();
            // log(format!("app event: {:?}", mess).as_str());
            self.driver.receive(mess.unwrap()).expect("could not receive");
        });

        listener
    }

    pub fn mount_to_body(
       self,
    ) -> gloo::events::EventListener {
        // Bootstrap the component for `Window` environment only (not for `Worker`)
        let element = document()
            .query_selector("body")
            .expect("can't get body node for rendering")
            .expect("can't unwrap body node");
        self.mount(element)
    }
}

/// Removes anything from the given element.
fn clear_element(element: &web_sys::Element) {
    while let Some(child) = element.last_child() {
        element.remove_child(&child).expect("can't remove a child");
    }
}
