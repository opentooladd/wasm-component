//! Test suite for the Web and headless browsers.

#![cfg(target_arch = "wasm32")]

extern crate wasm_bindgen_test;
use wasm_bindgen_test::*;

extern crate web_sys;
extern crate wasm_component;
use wasm_bindgen::JsValue;
use wasm_component::app::AppEvent;
use wasm_component::app::dom::{ DOMApp, DOMAppBuilder };

wasm_bindgen_test_configure!(run_in_browser);

#[wasm_bindgen_test]
fn create_app() {
    let builder: DOMAppBuilder = DOMApp::new(); 
    let app_id = builder.0.id.clone();
    let listener = DOMApp::init(builder.build());

    let app_event: AppEvent = AppEvent { 
        id: String::from(app_id.clone()), 
        event: "test_event".to_string() 
    };
    let window = web_sys::window().expect("could not get window");
    let event = web_sys::CustomEvent::new("app_message").unwrap();
    event.init_custom_event_with_can_bubble_and_cancelable_and_detail(
        format!("{}_app_message", app_id.clone()).as_str(),
        false,
        true,
        &JsValue::from_serde(&app_event).unwrap(),
    );

    match window.dispatch_event(&event) {
        Err(e) => {
            assert_eq!(true, false);
        }
        _ => {
            assert_eq!(true, true);
        }
    };
    listener.forget()
}
